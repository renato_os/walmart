<?php
    $_addresses = array(
        array('nome' => 'Renato Oliveira Silva','rua' => 'Rua Roque Bongiovani', 'numero' => 304, 'bairro' => 'Jardim Vila Real', 'cep' => '19063-360', 'cidade' => 'Presidente Prudente', 'uf' => 'SP', 'principal' => true),
        array('nome' => 'Prudenshopping','rua' => 'Av. Manoel Gourlart', 'numero' => 1.240, 'bairro' => 'Jardim das rosas', 'cep' => '19063-680', 'cidade' => 'Presidente Prudente', 'uf' => 'SP', 'principal' => false)
        );
?>

<div class="address-main">
    <div class="headline">
        <div class="title pull-left">
            <h1><span class="icon pull-left"></span>Endereços cadastrados</h1>
        </div>
    </div>
    <div class="address-list">
        <div class="new-address">
            <a href="javascript:void(0)">
                <span class="icon-add">+</span>
                Adicionar novo endereço
            </a>
        </div>
        <?php foreach($_addresses as $address): ?>
            <div class="panel-address panel">
                <span class="name"><?= $address['nome'] ?></span>
                <span class=""><?= $address['rua'] . ', ' . $address['numero'] ?></span>
                <span class=""><?= 'Bairro: ' . $address['bairro'] ?></span>
                <span class=""><?= 'CEP: ' . $address['cep'] ?></span>
                <span class=""><?= $address['cidade'] . ' - ' . $address['uf'] ?></span>
                <?php if($address['principal']): ?>
                <span class="main">Endereço principal</span>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
    </div>
</div>