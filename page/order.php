<?php
    /*** Simulação de recuperação do banco de dados ***/
    $_orders = array(
        array('pedido' => '12345678', 'data_pedido' => '22/03/2016', 'data_pgto' => '22/03/2016', 'data_transport' => '', 'data_saiu' => '', 'data_entrega' => '', 'previsao_entrega' => '11/04/2016', 'status' => 'pgto', 
                            'produtos' => array(array('nome' => 'TV LED 40” Samsung 40H5100 Full HD Função Futebol ConnectShare Movie', 'desc' => 'Assista em alta definição seus programas de TV e filmes favoritos na TV LED Samsung de 40 polegadas. O produto conta com o recurso Clean View, que remove os ruídos digitais e analógicos da tela.', 'img' => 'tv_samsung.jpg', 'valor' => '1.648,00', 'qnt' => '1'), 
                                                array('nome' => 'Smartphone Samsung Galaxy S7 Edge SM-G935F Preto Single Chip Android 6.0 Marshmallow 4G Wi-Fi Câmera Dual Pixel 12MP Octa-Core e API Vulkan', 'desc' => 'O Samsung Galaxy S7 Edge é um smartphone com ampla tela curva de 5.5”, que combina design ergonômico e sofisticação de peças, com hardware de última geração na completa plataforma do sistema Android™ 6.0 Marshmallow, ideal para desempenhar múltiplas funções que exigem alto desempenho de velocidade e qualidade gráfica.', 'img' => 'samsung_galaxy_s7.jpg', 'valor' => '4.299,00', 'qnt' => '1'))),
        
         array('pedido' => '87654321', 'data_pedido' => '12/01/2016', 'data_pgto' => '12/01/2016', 'data_transport' => '15/01/2016', 'data_saiu' => '25/01/2016', 'data_entrega' => '25/01/2016', 'previsao_entrega' => '27/01/2016', 'status' => 'entregue', 
            'produtos' => array(array('nome' => 'Console Playstation 4 500GB', 'desc' => 'O sistema PS4™ foca nos jogadores, garantindo que os melhores jogos e a experiência mais imersiva seja possível na plataforma. O sistema PS4™ permite que os melhores desenvolvedores de jogos do mundo abram sua criatividade e ultrapassem os limites do jogo através de um sistema ajustado especificamente para suas necessidades. O sistema PS4™ é centrado em um poderoso chip personalizado que contém oito núcleos x86-64 e um tecnológico processador gráfico 1.84 TFLOPS com um sistema unificado de memória GDDR5 ultra rápido de 8 GB, facilitando a criação de jogos e aumentando a riqueza do conteúdo possível na plataforma. O resultado final são novos jogos, com gráficos ricos de alta fidelidade e experiência profundamente imersivas.', 'img' => 'ps4.jpg', 'valor' => '1.849,99', 'qnt' => '1')))
    );
?>

<div class="order-main">
    <div class="headline">
        <div class="title pull-left">
            <h1><span class="icon pull-left"></span>Pedidos</h1>
        </div>        
    </div>
    <div class="form-search">
            <input id="search" type="text" class="input-text" value="Número do pedido"/>
            <button type="submit" title="Pesquisa por" class="button"></button>
        </div>
    <div class="products-list">
        <?php foreach($_orders as $order): ?>
            <div class="panel-order panel">
                <div class="order-info pull-left">
                    <span class="order-title">Nº do pedido:</span>
                    <span class="order-id"><?= $order['pedido'] ?></span>
                    <span class="order-date">Pedido realizado em: <?= $order['data_pedido'] ?></span>
                    <?php if($order['data_entrega'] == ''): ?>
                    <span class="order-date prediction">Previsão de entrega: <?= $order['previsao_entrega'] ?></span>
                    <?php else:?>
                    <span class="order-date delivered">Entrega realizada em: <?= $order['data_entrega'] ?></span>
                    <?php endif; ?>
                </div>
                <div class="order-product pull-left">
                    <?php foreach($order['produtos'] as $produto): ?>
                    <div class="product-image">
                        <img src="image/product/<?=$produto['img'] ?>" alt="" width="90" height="90" />
                        <div class="product-popup" style="display: none">
                            <span class="product-name"><?= $produto['nome'] ?></span>
                            <span class="product-desc"><?= $produto['desc'] ?></span>
                            <span class="product-price">Valor: R$<?= $produto['valor'] ?></span>
                            <span class="product-qty">Quantidade: <?= $produto['qnt'] ?></span>
                            <span class="product-subtotal">Subtotal: R$<?= $produto['valor']*$produto['qnt'] ?></span>
                        </div>                        
                    </div>
                    <?php endforeach; ?>
                </div>                
            </div>
            <?php /* implementação da "linha do tempo/rastreamento" do pedido, caso haja tempo.
                <div class="time-line">                
                </div>
            */ ?>
        <?php endforeach; ?>
    </div>
</div>
<script type="text/javascript">           
    $jQ(function() {
        $jQ('.form-search input[type=text]').focus(function() {
            $jQ(this).val('');
        }).focusout(function() {
            $jQ(this).val('Número do pedido');
        })
    });
    
    $jQ(document).ready(function(){
        $jQ('.product-image img').hover(function(){
            $jQ(this).next().addClass("active");                  
        }, function() {
            $jQ(this).next().removeClass("active");
        });
    });
</script>