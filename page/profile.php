<?php
    $_profile = array('nome' => 'Renato Oliveira Silva', 'telefone' => '(18) 99758-5494', 'cpf' => '123.***.***-**', 'email' => 'renato-oliveira@outlook.com.br', 'sexo' => 'Masculino', 'senha' => '*******', 'dt_nasc' => '01/01/1901');
?>

<div class="profile-main">
    <div class="headline">
        <div class="title pull-left">
            <h1><span class="icon pull-left"></span>Dados pessoais</h1>
        </div>
    </div>
    <div class="profile-list">
        <div class="panel-profile panel">
            <div class="panel-left pull-left">
                <span class="title">Nome</span>
                <span class="data"><?= $_profile['nome'] ?></span>
                <span class="title">CPF</span>
                <span class="data"><?= $_profile['cpf'] ?></span>
                <span class="title">Sexo</span>
                <span class="data"><?= $_profile['sexo'] ?></span>
                <span class="title">Data de Nascimento</span>
                <span class="data"><?= $_profile['dt_nasc'] ?></span>
            </div>
            <div class="panel-right pull-right">
                <span class="title">Telefone</span>
                <span class="data"><?= $_profile['telefone'] ?></span>
                <span class="title">E-mail</span>
                <span class="data"><?= $_profile['email'] ?></span>
                <span class="title">Senha</span>
                <span class="data"><?= $_profile['senha'] ?></span>
                <input type="checkbox" name="newsletter" id="newsletter" checked="checked" />
                <span class="newsletter">Desejo receber e-mails com novidades e promoções</span>
            </div>
        </div>
    </div>
</div>

<?php 
    require 'address.php'; 
    require 'order.php'; 
?>


