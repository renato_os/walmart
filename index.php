<!DOCTYPE html>
<html lang="pt-br">
    <head>        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="./css/style.css">        
        <title>Walmart.com</title>
    </head>
    <?php         
        $_menu = array(
            array('title' => 'Seu Perfil', 'class' => 'profile'),
            array('title' => 'Pedidos', 'class' => 'order'),
            array('title' => 'Endereços', 'class' => 'address')
        );
    ?>
    <body>
        <div class="wrapper">
            <div class="container">
                <div class="header-container">
                    <div class="header">
                        <img src="image/header.png" alt="" />
                    </div>
                </div>
            </div>
            <div class="container main">
                    <div class="pull-left sidebar">
                        <div class="profile">
                            <!-- <span class="icon-picture pull-left"></span> -->
                            <img src="image/picture.png" alt="" width="100" height="100" class="pull-left" />
                            <div class="welcome pull-left">  
                                <span class="text">Olá,</span>
                                <span class="profile-name text pull-right">Renato</span>
                            </div>
                        </div>
                        <div class="menu">
                            <ul>
                                <?php foreach ($_menu as $item): ?>
                                <li class="<?= $item['class'] ?>">
                                    <div class="icon">  
                                        <span></span>
                                    </div>
                                    <div class="link">
                                        <a href="javascript:void(0)" id="<?= $item['class'] ?>"><?= $item['title'] ?></a>
                                    </div>
                                </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>                            
                    </div>
                    <div class="col-main">

                    </div>
            </div>
            <div class="footer-wrapper"> 
                <div class="container">      
                    <img src="image/footer.png" alt="" />
                </div>
            </div>
        </div>
        <script type="text/javascript" src="./js/jquery-2.2.2.min.js"></script>
        <script type="text/javascript" src="./js/bootstrap.min.js"></script>
        <script type="text/javascript">
            $jQ = jQuery.noConflict();
            
            $jQ(document).ready(function(){
                $jQ('.menu ul li a').hover(function(){
                    $jQ(this).parents('li').addClass("hover");                  
                }, function() {
                    $jQ(this).parents('li').removeClass("hover");
                });
                
                $jQ('.menu ul li a').click(function(){
                    $jQ('.menu ul li').each(function(){
                        $jQ(this).removeClass("active");
                    });
                    
                    $jQ(this).parents('li').addClass("active");
                    $jQ(".col-main").load("page/" + $jQ(this).attr('id') + ".php");
                });
              
               $jQ("#profile").click();
            });
        </script>
    </body>
</html>
